## YAJD: Yet Another Joycon Driver (for Linux)

*But hey, this time it's in Rust and the code is somewhat readable, yay.*

A Linux uinput driver that communicates with the Switch Controllers over USB.

Before running, make sure to `sudo modprobe uinput`.

This is my first ever Rust project and I learned the language with this, so expect some rough edges here and there.

![](https://elixi.re/i/asitepj7.png)

![](https://elixi.re/i/u4vr95ya.png)

#### Limitations and goals.

Currently only Pro Controller over USB is supported.

I plan to add Joycon and Pro Controller support over BT, alongside SPI dumping, vibrations, gyro reading and LED magic.

For now, I recommend using the [Go joycon driver by riking](https://github.com/riking/joycon) to use joycons through BT, I've used it for a long time before getting myself a Pro Controller and it's absolutely amazing.

#### Credits

- nacabaro for the name idea (I was going to call it "rust-jc" otherwise).
- jeikabu for [this article](https://dev.to/jeikabu/gamepad-input-with-rust-2oji) which helped me a lot with figuring out the basics of hidapi.
- shinyquagsire for [HID-Joy-Con-Whispering](https://github.com/shinyquagsire23/HID-Joy-Con-Whispering), which helped me figure out handshakes (ps. your SPI dump code is broken and gives a 2^19 + 3 byte output instead of 2^19).
- ctcAER for [jc_toolkit](https://github.com/CTCaer/jc_toolkit/), which helped me figure out some other features of joycons, such as SPI dumping, rumble and LEDs.

