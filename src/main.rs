extern crate hidapi;
extern crate uinput;

use std::collections::HashMap;
use uinput::event::controller::{GamePad, DPad};
use uinput::event::absolute::Position;

fn switch_exchange(joycon: &hidapi::HidDevice, buffer: &[u8]) -> [u8; 400] {
    // Send given buffer to the joycon
    joycon.write(&buffer).unwrap();
    // println!("Sent: {:X?}", &buffer);

    // Receive response
    let mut buf = [0u8; 400];

    // let res = joycon.read_timeout(&mut buf[..], 100).unwrap();
    // if res > 22 && buf[0] == 0x81 && buf[1] == 0x92 {
    //     println!("Reply: {:X?}", &buf[8..22]);
    // }

    joycon.read(&mut buf[..]).unwrap();
    // TODO: Return res too
    return buf;
}

fn switch_command(joycon: &hidapi::HidDevice, command: u8, buffer: &[u8]) -> [u8; 400] {
    let mut command_data = vec![0x80, 0x92, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, command];
    command_data.extend_from_slice(&buffer);
    return switch_exchange(&joycon, &command_data);
}

fn switch_subcommand(joycon: &hidapi::HidDevice, command: u8, subcommand: u8, buffer: &[u8]) -> [u8; 400] {
    let mut command_data = vec![0x0, 0x00, 0x01, 0x40, 0x40, 0x00, 0x01, 0x40, 0x40, subcommand];
    command_data.extend_from_slice(&buffer);
    return switch_command(&joycon, command, &command_data);
}

fn switch_init(joycon: &hidapi::HidDevice) {
    // Initialize joycon and print MAC address
    let buf = switch_exchange(&joycon, &[0x80, 0x01]);
    println!("MAC: {:X}:{:X}:{:X}:{:X}:{:X}:{:X}", &buf[9], &buf[8], &buf[7], &buf[6], &buf[5], &buf[4]);

    // Do handshaking
    switch_exchange(&joycon, &[0x80, 0x02]);

    // Switch baudrate
    switch_exchange(&joycon, &[0x80, 0x03]);

    // Do handshake again
    switch_exchange(&joycon, &[0x80, 0x02]);

    // Only do HID from now on
    switch_exchange(&joycon, &[0x80, 0x04]);

    // Enable vibration
    switch_subcommand(&joycon, 0x1, 0x48, &[0x1]);

    // Enable IMU data (gyro)
    // switch_subcommand(&joycon, 0x1, 0x40, &[0x1]);

    // TODO: LEDs
    // TODO: Vibration
    // TODO: Dumping SPI
    // TODO: Battery, Color
    // TODO: Calibration
}

fn clean_stick(x: u16, y: u8) -> (i32, i32) {  // AKA Binyot, according to merve.
    let x_base = 0xFFF / 2;
    let y_base = (u8::max_value() / 2) as i32;

    // Inner deadzone size is set here (0.15)
    let inner_deadzone = i16::max_value() as f32 * 0.15 as f32;

    // Magic number, this amount gets me closest to the maximum value we want (32767)
    let x_diff = (80000 as u32 / 0xFFF) as i32;
    let y_diff = (80000 as u32 / u8::max_value() as u32) as i32;

    let mut x_val = ((x as i32) - x_base) * x_diff;
    let mut y_val = ((y as i32) - y_base) * y_diff;

    // sqrt((x2-x1)²+(y2-y1)²), where y1 and x1 are just 0 (base)
    let distance_from_base = (((x_val * x_val) + (y_val * y_val)) as f32).sqrt();

    // Handle inner deadzone
    if inner_deadzone > distance_from_base {
        x_val = 0;
        y_val = 0;
    }

    // TODO: Adjust by calibration data
    // TODO: Handle outer deadzones
    (
        x_val,
        y_val
    )
}

fn switch_input(joycon: &hidapi::HidDevice, uinput_device: &mut uinput::Device) {
    // TODO: move the maps to a shared space so that we don't init them every single time
    // Keys: 0x1 on Y, 0x2 on X, 0x4 on B, 0x8 on A, 0x40 on R, 0x80 on ZR
    // https://www.kernel.org/doc/html/latest/input/gamepad.html
    // https://www.kernel.org/doc/html/latest/input/joydev/joystick.html
    let keymap: HashMap<u8, &uinput::event::controller::GamePad> =
    [(0x8, &GamePad::East),
     (0x4, &GamePad::South),
     (0x2, &GamePad::North),
     (0x1, &GamePad::West),
     (0x40, &GamePad::TR),
     (0x80, &GamePad::TR2)]
     .iter().cloned().collect();

    // Face buttons are 0x80 by default. - is 0x1, + is 0x2, right thumb press down is 0x4, left thumb press down is 0x8, home is 0x10, capture is 0x20.
    let fbuttonmap: HashMap<u8, &uinput::event::controller::GamePad> =
    [(0x1, &GamePad::Select),
     (0x2, &GamePad::Start),
     (0x4, &GamePad::ThumbR),
     (0x8, &GamePad::ThumbL),
     (0x10, &GamePad::Mode),
     (0x20, &GamePad::Z)]  // I have no idea what to map capture to, so it gets to be Z 
     .iter().cloned().collect();

    // DPad: 0x1 on down, 0x2 on up, 0x6 on right, 0x8 on left, 0x40 on L, 0x80 on ZL
    let dmapmap: HashMap<u8, &uinput::event::controller::DPad> =
    [(0x1, &DPad::Down),
     (0x2, &DPad::Up),
     (0x6, &DPad::Right),
     (0x8, &DPad::Left)]
     .iter().cloned().collect();

    let dmapmap2: HashMap<u8, &uinput::event::controller::GamePad> =
    [(0x40, &GamePad::TL),
     (0x80, &GamePad::TL2)]
     .iter().cloned().collect();

    let input = switch_command(&joycon, 0x1f, &[]);
    // TODO: Check the whole header
    if input[0] == 0x81 && input[1] == 0x92 {
        let keys = input[13];
        let face_buttons = input[14] - 0x80;
        let dpad = input[15];

        // X axis code is based on https://github.com/CTCaer/jc_toolkit/blob/master/jctool/jctool.cpp#L1011
        let (lthumb_x, lthumb_y) = clean_stick(input[16] as u16 | (((input[17] & 0xF) as u16) << 8), input[18]);
        let (rthumb_x, rthumb_y) = clean_stick(input[19] as u16 | (((input[20] & 0xF) as u16) << 8), input[21]);

        uinput_device.position(&Position::X, lthumb_x).unwrap();
        uinput_device.position(&Position::Y, lthumb_y).unwrap();
        uinput_device.position(&Position::RX, rthumb_x).unwrap();
        uinput_device.position(&Position::RY, rthumb_y).unwrap();

        // println!("Keys: {:X}, FButtons: {:X}, DPad: {:X}", keys, face_buttons, dpad);
        // println!("LThumb: X:{:?} Y:{:?}, RThumb: X:{:?} Y:{:?}", lthumb_x, lthumb_y, rthumb_x, rthumb_y);

        // Keys (A B X Y R ZR)
        for (keyval, &uinputval) in &keymap {
            if keys & keyval != 0 {
                uinput_device.press(uinputval).unwrap();
            }
            else {
                uinput_device.release(uinputval).unwrap();
            }
        }

        // Face Buttons (- + Capture Home LThumb RThumb)
        for (keyval, &uinputval) in &fbuttonmap {
            if face_buttons & keyval != 0 {
                uinput_device.press(uinputval).unwrap();
            }
            else {
                uinput_device.release(uinputval).unwrap();
            }
        }

        // DPad (DPad L ZL)
        for (keyval, &uinputval) in &dmapmap {
            if dpad & keyval != 0 {
                uinput_device.press(uinputval).unwrap();
            }
            else {
                uinput_device.release(uinputval).unwrap();
            }
        }

        // Other buttons on the same byte as DPad (L ZL)
        for (keyval, &uinputval) in &dmapmap2 {
            if dpad & keyval != 0 {
                uinput_device.press(uinputval).unwrap();
            }
            else {
                uinput_device.release(uinputval).unwrap();
            }
        }
        uinput_device.synchronize().unwrap();
    }
}

fn main() {
    // TODO: https://github.com/meh/rust-uinput
    // https://docs.rs/uinput/0.1.3/uinput/event/controller/enum.GamePad.html
    // https://docs.rs/uinput/0.1.3/uinput/event/absolute/enum.Position.html
    println!("Hewwo, wewcome to YAJD :3");
    println!("Released under GPLv3, https://gitlab.com/ao/yajd");

    let mut jc = uinput::default().unwrap()
        .name("YAJD").unwrap()
        .event(GamePad::East).unwrap()
        .event(GamePad::West).unwrap()
        .event(GamePad::South).unwrap()
        .event(GamePad::North).unwrap()
        .event(GamePad::TR).unwrap()
        .event(GamePad::TR2).unwrap()
        .event(GamePad::Select).unwrap()
        .event(GamePad::Start).unwrap()
        .event(GamePad::ThumbR).unwrap()
        .event(GamePad::ThumbL).unwrap()
        .event(GamePad::Mode).unwrap()
        .event(GamePad::Z).unwrap()
        .event(DPad::Up).unwrap()
        .event(DPad::Down).unwrap()
        .event(DPad::Left).unwrap()
        .event(DPad::Right).unwrap()
        .event(GamePad::TL).unwrap()
        .event(GamePad::TL2).unwrap()
        .event(Position::X).unwrap()
        .event(Position::Y).unwrap()
        .event(Position::RX).unwrap()
        .event(Position::RY).unwrap()
        .create().unwrap();

    // TODO: Perhaps map screenshot key to PrintScreen

    let api = hidapi::HidApi::new().unwrap();

    println!("Connecting...");
    // Connect to Pro Controller
    let (vid, pid) = (0x057e, 0x2009);
    let joycon = api.open(vid, pid).unwrap();

    switch_init(&joycon);
    println!("Connected, sending data to uinput~");

    loop {
        // Read data from joycon and give to uinput
        switch_input(&joycon, &mut jc);
        // std::thread::sleep(std::time::Duration::from_millis(100));
    }

    // TODO: Deinit JC
}
